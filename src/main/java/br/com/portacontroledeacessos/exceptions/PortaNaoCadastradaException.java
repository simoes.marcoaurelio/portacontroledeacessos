package br.com.portacontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não cadastrada no sistema")
public class PortaNaoCadastradaException extends RuntimeException {
}
