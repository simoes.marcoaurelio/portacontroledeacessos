package br.com.portacontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Não existem portas cadastradas")
public class ListaDePortasVaziaException extends RuntimeException {
}
