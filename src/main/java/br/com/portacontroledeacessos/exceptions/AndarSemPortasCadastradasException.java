package br.com.portacontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O andar informado não possui portas cadastradas")
public class AndarSemPortasCadastradasException extends RuntimeException{
}
