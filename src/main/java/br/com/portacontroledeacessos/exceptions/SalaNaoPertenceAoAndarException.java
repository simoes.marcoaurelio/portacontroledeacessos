package br.com.portacontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "A sala não pertence ao andar informado")
public class SalaNaoPertenceAoAndarException extends RuntimeException {
}
