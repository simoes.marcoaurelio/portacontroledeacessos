package br.com.portacontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Porta já cadastrada no sistema")
public class PortaJaCadastradaException extends RuntimeException {
}
