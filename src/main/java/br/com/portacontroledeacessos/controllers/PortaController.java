package br.com.portacontroledeacessos.controllers;

import br.com.portacontroledeacessos.models.Porta;
import br.com.portacontroledeacessos.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta cadastrarPorta(@RequestBody @Valid Porta porta) {
        return portaService.cadastrarPorta(porta);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Porta consultarPortaPorId(@PathVariable int id) {
        return portaService.consultarPortaPorId(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public Iterable<Porta> consultarListaDePortas(@RequestParam(name = "andar", required = false) String andar) {
        if (andar != null) {
            return portaService.consultarPortasPorAndar(andar);
        }
        else {
            return portaService.consultarTodasAsPortas();
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deltarPortaPorId(@PathVariable int id) {
        portaService.deletarPortaPorId(id);
    }
}
