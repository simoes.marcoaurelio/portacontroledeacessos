package br.com.portacontroledeacessos.repositories;

import br.com.portacontroledeacessos.models.Porta;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
    Optional<Porta> findBySala(String sala);
    Iterable<Porta> findAllByAndar(String andar);
}
