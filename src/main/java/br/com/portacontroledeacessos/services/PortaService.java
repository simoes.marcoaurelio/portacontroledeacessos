package br.com.portacontroledeacessos.services;

import br.com.portacontroledeacessos.exceptions.*;
import br.com.portacontroledeacessos.models.Porta;
import br.com.portacontroledeacessos.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta cadastrarPorta(Porta porta) {
        verificarSePortaExiste(porta.getSala());
        validarSeSalaPertenceAoAndar(porta.getSala(), porta.getAndar());
        return portaRepository.save(porta);
    }

    public void verificarSePortaExiste(String sala) {
        Optional<Porta> optionalPorta = portaRepository.findBySala(sala);
        if (optionalPorta.isPresent()) {
            throw new PortaJaCadastradaException();
        }
    }

    public void validarSeSalaPertenceAoAndar(String sala, String andar) {
        String andarDaSala = sala.substring(0, 2);

        if (andar.equals(andarDaSala)) {
            return;
        }
        else {
            throw new SalaNaoPertenceAoAndarException();
        }
    }

    public Porta consultarPortaPorId(int id) {
        Optional<Porta> optionalPorta = portaRepository.findById(id);

        if (!optionalPorta.isPresent()) {
            throw new PortaNaoCadastradaException();
        }
        return optionalPorta.get();
    }

    public Iterable<Porta> consultarTodasAsPortas() {
        Iterable<Porta> portaIterable =  portaRepository.findAll();
        verificaListaDePortasVazia(portaIterable);
        return portaIterable;
    }

    public Iterable<Porta> consultarPortasPorAndar(String andar) {
        Iterable<Porta> portaIterable = portaRepository.findAllByAndar(andar);
        verificaListaDePortasVazia(portaIterable);
        return portaIterable;
    }

    public void verificaListaDePortasVazia(Iterable<Porta> portaIterable) {
        int contador = 0;
        for (Porta portaObjeto : portaIterable) {
            contador++;
        }
        if (contador == 0) {
            throw new ListaDePortasVaziaException();
        }
    }

    public void deletarPortaPorId(int id) {
        if (portaRepository.existsById(id)){
            portaRepository.deleteById(id);
        }
        else {
            throw new PortaNaoCadastradaException();
        }
    }
}
