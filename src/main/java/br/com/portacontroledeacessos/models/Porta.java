package br.com.portacontroledeacessos.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 2, max = 2,
            message = "Insira o andar do edificio corretamente, utilizando 2 caracteres (Ex.: 08)")
    private String andar;

    @Size(min = 5, max = 5,
            message = "Insira a identificação da sala corretamente, utilizando 5 caracteres (Ex.: 11B35)")
    private String sala;

    public Porta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
