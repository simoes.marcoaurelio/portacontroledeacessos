package br.com.portacontroledeacessos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortaControleDeAcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortaControleDeAcessosApplication.class, args);
	}

}
